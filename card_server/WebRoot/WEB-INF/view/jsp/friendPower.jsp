<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'friendPower.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
	<script type="text/javascript">
	
		function check()
		{
			var reg = new RegExp("^[0-9]*$");  
			var st = document.getElementById("sendTime");
			if(!reg.test(st.value) || st.value==null || st.value=="")
			{  
				alert("请输入数字!");
				st.focus(); 
				return; 
			} 
			var sp = document.getElementById("sendPower");
			if(!reg.test(sp.value) || sp.value==null || sp.value=="")
			{
				alert("请输入数字!");
				sp.focus(); 
				return; 
			}
			document.form1.submit();
		}
		
		function clearMsg()
		{
			document.getElementById("msgTd").innerHTML="";
		}
	</script>
	
  </head>
  
  <body>
	<form action="gm.htm?action=modFriendPower" method="post" name="form1">
	    <table align="center">
	    	<tr align="center">
	    		<td>每日接收好友体力的次数上限</td>
	    		<td><input type="text" name="sendTime" id="sendTime" value="${sendTime}" onfocus="clearMsg()"/></td>
	    	</tr>
	    	<tr align="center">
	    		<td>每次接收的体力数</td>
	    		<td><input type="text" name="sendPower" id="sendPower" value="${sendPower}" onfocus="clearMsg()"/></td>
	    	</tr>
	    	<tr align="center">
				<td colspan="2"> <input type="button" value="点击修改" onclick="javascript:check()"/> </td>    	
	    	</tr>
	    	<tr align="center">
	    		<td colspan="2" id="msgTd"> <font color="green"><b>${msg}</b></font></td>
	    	</tr>
	    </table>
    </form>
  </body>
</html>
