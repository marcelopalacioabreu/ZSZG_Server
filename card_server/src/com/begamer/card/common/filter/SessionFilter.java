package com.begamer.card.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.begamer.card.common.Constant;
import com.begamer.card.model.pojo.Manager;

/**
 * 
 * @ClassName: SessionFilter
 * @Description:用户session过滤器
 * @author gs
 * @date Nov 10, 2011 4:08:12 PM
 * 
 */
public class SessionFilter implements Filter {

	public static String[] except = { 
		"user.htm",
		"player.htm",
		"battle.htm",
		"battleReady.htm",
		"ui.htm",
		"rune.htm",
		"imagination.htm",
		"friend.htm",
		"pk.htm",
		"event.htm",
		"achieve.htm",
		"buy.htm",
		"activity.htm",
		"receive_pay.htm",
		"announce.htm",
		/**gm页**/
		"portal.htm",
		"gmLogin.htm",
	};

	public void destroy()
	{
		// Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException
	{
		// 获取账号信息UserInfo
		// logger.debug("run session filter");
		HttpServletRequest req = (HttpServletRequest) request;
		// logger.debug("req.getRequestURI():" + req.getRequestURI());
		for (int i = 0; i < except.length; i++)
		{
			if (req.getRequestURI().indexOf(except[i], 0) >= 0)
			{
				// logger.debug("is containt!");
				chain.doFilter(request, response);
				return;
			}
		}
		HttpSession session = req.getSession();
		Object obj = session.getAttribute(Constant.LOGIN_USER);
		if (null != obj)
		{
			Manager manager = (Manager) obj;
			if (manager != null)
			{
				request.setAttribute(Constant.LOGIN_USER, manager);
			}
			else
			{
				// res.sendRedirect(this.indexPage);
				java.io.PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>");
				out.println("<script>");
				out.println("alert('您登录超时，请重新登录！')");
				out.println("window.open ('gmLogin.htm?action=index', 'father')");
				out.println("</script>");
				out.println("</html>");
				return;
			}
		}
		else
		{
			java.io.PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>");
			out.println("<script>");
			out.println("alert('您登录超时，请重新登录！')");
			out.println("window.open ('gmLogin.htm?action=index', 'father')");
			out.println("</script>");
			out.println("</html>");
			return;
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig arg0) throws ServletException
	{
		// Auto-generated method stub

	}

}
