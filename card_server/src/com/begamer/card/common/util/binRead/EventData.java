package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class EventData implements PropertyReader {
	/** 编号 **/
	public int ID;
	/** 位置编号 **/
	public int positionid;
	/** 时间类型 **/
	public int timestyle;
	/**
	 * 名称类型 1，死亡洞窟 2，其他
	 **/
	public int nametype;
	/** 活动名称 **/
	public String name;
	/** 活动描述 **/
	public String content;
	/** 活动图片 **/
	public String image;
	/** 图集 **/
	public String atlas;
	/** 种族图标 **/
	public int raceimage;
	/** 副本编号 **/
	public List<Integer> fbid;
	
	private static HashMap<Integer, EventData> data = new HashMap<Integer, EventData>();
	private static List<EventData> dataList = new ArrayList<EventData>();
	
	@Override
	public void addData()
	{
		data.put(ID, this);
		dataList.add(this);
	}
	
	@Override
	public void parse(String[] ss)
	{
		int location = 0;
		ID = StringUtil.getInt(ss[location]);
		positionid = StringUtil.getInt(ss[location + 1]);
		timestyle = StringUtil.getInt(ss[location + 2]);
		nametype = StringUtil.getInt(ss[location + 3]);
		name = StringUtil.getString(ss[location + 4]);
		content = StringUtil.getString(ss[location + 5]);
		image = StringUtil.getString(ss[location + 6]);
		atlas = StringUtil.getString(ss[location + 7]);
		raceimage = StringUtil.getInt(ss[location + 8]);
		fbid = new ArrayList<Integer>();
		location = 9;
		for (int k = 0; k < 5; k++)
		{
			int Fbid1 = StringUtil.getInt(ss[location + k]);
			if (Fbid1 != 0)
			{
				fbid.add(Fbid1);
			}
			else
			{
				continue;
			}
		}
		addData();
	}
	
	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	
	/** 根据活动编号获取eventdata **/
	public static EventData getEventData(int index)
	{
		return data.get(index);
	}
	
	/** 所有副本 **/
	public static List<EventData> getEvents()
	{
		return dataList;
	}
}
