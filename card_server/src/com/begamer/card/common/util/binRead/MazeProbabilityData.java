package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class MazeProbabilityData implements PropertyReader
{
	public int id;
	public int mazeID;
	public int battlestyle;
	public int awardfirst;//首杀奖励
	public int exp1;
	public int expprobability1;
	public int gold1;
	public int goldprobability1;
	public List<String> drop_pro;
	private static HashMap<Integer, MazeProbabilityData> data =new HashMap<Integer, MazeProbabilityData>();
	@Override
	public void addData() {
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) {
		int location =0;
		id=StringUtil.getInt(ss[location]);
		mazeID =StringUtil.getInt(ss[location+1]);
		battlestyle =StringUtil.getInt(ss[location+2]);
		awardfirst =StringUtil.getInt(ss[location+3]);
		drop_pro =new ArrayList<String>();
		for(int i=0;i<21;i++)
		{
			location =4+i*3;
			int dropID =StringUtil.getInt(ss[location]);
			if(dropID==0)
			{
				continue;
			}
			int probability =StringUtil.getInt(ss[location+1]);
			int look =StringUtil.getInt(ss[location+2]);
			String dropIDpro =dropID+"-"+probability+"-"+look;
			drop_pro.add(dropIDpro);
		}
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
	}
	
	/**根据迷宫id和战斗类型获取data**/
	public static MazeProbabilityData getMpData(int map , int type)
	{
		for(MazeProbabilityData mpData : data.values())
		{
			if(mpData.mazeID ==map && mpData.battlestyle ==type)
			{
				return mpData;
			}
		}
		return null;
	}
}
