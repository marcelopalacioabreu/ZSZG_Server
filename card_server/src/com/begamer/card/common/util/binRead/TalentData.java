package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class TalentData implements PropertyReader
{
	public int index;
	public String name;
	/**1对技能类型加成,2对元素,3对士气槽,4对特定技能,5对本方单位,6对敌方卡牌,7特殊**/
	public int type;
	/**对于每个type都有不同的意义**/
	public int class1;
	public int effect;
	public float number;
	public int action;
	public String icon;
	public String description;

	private static HashMap<Integer, TalentData> data=new HashMap<Integer, TalentData>();
	
	public void addData()
	{
		data.put(index,this);
	}
	public void resetData()
	{
		data.clear();
	}
	public void parse(String[] ss)
	{}
	
	public static TalentData getData(int talentId)
	{
		return data.get(talentId);
	}

}
