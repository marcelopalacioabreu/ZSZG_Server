package com.begamer.card.json;

import java.util.ArrayList;
import java.util.List;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.UniteskillrobotData;
import com.begamer.card.model.pojo.Player;

public class FriendElement
{
	/**好友等级**/
	public int level;
	/**好友名字**/
	public String name;
	/**合体技Id**/
	public int unit;
	/**上次登录时间,yyyy-MM-dd HH:mm:ss**/
	public String login;
	/**好友状态:好友状态:0未赠不可收,1已删除,2未赠可收,3未赠已收,4已赠不可收,5已赠可收,6已赠已收
	 * 申请状态:0已经是好友,1未申请,2已申请
	 * 援护玩家:0好友,3陌生人**/
	public int t;
	//==玩家id==//
	public int pid;
	//==玩家icon==//
	public String icon;
	//==玩家战力==//
	public int bp;
	
	/**获取初始援护数据**/
	public static FriendElement getOrl()
	{
		List<UniteskillrobotData> usds=UniteskillrobotData.getDatas();
		UniteskillrobotData usd=usds.get(usds.size()-1);
		FriendElement fe=new FriendElement();
		fe.level=usd.level;
		fe.name=usd.name;
		fe.unit=usd.uniteskill;
		fe.login=StringUtil.getDateTime(System.currentTimeMillis());
		fe.t=3;
		fe.pid=usds.size();
		fe.icon="card042";
		fe.bp=usd.power;
		fe.setDateTime();
		return fe;
	}
	
	/**获取初始援护数据**/
	public static List<FriendElement> getInitFriendElements()
	{
		List<FriendElement> list=new ArrayList<FriendElement>();
		List<UniteskillrobotData> usds=UniteskillrobotData.getDatas();
		int num=0;
		for(int k=usds.size()-1;num<5 && k>=0;k--)
		{
			num++;
			FriendElement fe=new FriendElement();
			UniteskillrobotData usd=usds.get(k);
			fe.level=usd.level;
			fe.name=usd.name;
			fe.unit=usd.uniteskill;
			fe.login=StringUtil.getDateTime(System.currentTimeMillis());
			fe.t=3;
			fe.pid=k+1;
			fe.icon="card042";
			fe.bp=usd.power;
			fe.setDateTime();
			
			list.add(fe);
		}
		return list;
	}
	
	public FriendElement(){}
	
	public FriendElement(Player p,int state)
	{
		level=p.getLevel();
		name=p.getName();
		unit=p.getIconId();
		login=p.getLastLogin();
		t=state;
		pid=p.getId();
		icon=p.getHead();
		bp=p.getBattlePower();
		setDateTime();
	}
	
	public FriendElement(int playerId,int state)
	{
		Player p=Cache.getInstance().getPlayer(playerId);
		level=p.getLevel();
		name=p.getName();
		unit=p.getIconId();
		login=p.getLastLogin();
		t=state;
		pid=p.getId();
		icon=p.getHead();
		bp=p.getBattlePower();
		setDateTime();
	}
	
	public FriendElement(int playerId,int state,int unitId)
	{
		Player p=Cache.getInstance().getPlayer(playerId);
		level=p.getLevel();
		name=p.getName();
		unit=unitId;
		login=p.getLastLogin();
		t=state;
		pid=p.getId();
		icon=p.getHead();
		bp=p.getBattlePower();
		setDateTime();
	}
	
	public FriendElement(int playerId,int state,String time)
	{
		Player p=Cache.getInstance().getPlayer(playerId);
		level=p.getLevel();
		name=p.getName();
		unit=p.getIconId();
		login=time;
		t=state;
		pid=p.getId();
		icon=p.getHead();
		bp=p.getBattlePower();
		setDateTime();
	}
	
	public int getLevel()
	{
		return level;
	}
	public void setLevel(int level)
	{
		this.level = level;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public int getUnit()
	{
		return unit;
	}
	public void setUnit(int unit)
	{
		this.unit = unit;
	}
	public String getLogin()
	{
		return login;
	}
	public void setLogin(String login)
	{
		this.login = login;
	}
	public int getT()
	{
		return t;
	}
	public void setT(int t)
	{
		this.t = t;
	}
	public int getPid()
	{
		return pid;
	}
	public void setPid(int pid)
	{
		this.pid = pid;
	}
	public String getIcon()
	{
		return icon;
	}
	public void setIcon(String icon)
	{
		this.icon = icon;
	}
	public int getBp()
	{
		return bp;
	}
	public void setBp(int bp)
	{
		this.bp = bp;
	}

	private void setDateTime()
	{
		long timeStamp=StringUtil.getTimeStamp(login);
		long now=System.currentTimeMillis();
		long offset=(now-timeStamp)/1000/60;
		if(offset>=365*24*60)
		{
			login=offset/(365*24*60)+"年前";
		}
		else if(offset>=24*60)
		{
			login=offset/(24*60)+"天前";
		}
		else if(offset>=60)
		{
			login=offset/60+"小时前";
		}
		else
		{
			login=offset+"分钟前";
		}
	}
	
}
