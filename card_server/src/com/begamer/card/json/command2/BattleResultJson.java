package com.begamer.card.json.command2;

import com.begamer.card.json.CardJson;
import com.begamer.card.json.ErrorJson;

public class BattleResultJson extends ErrorJson
{
	public int md;//missionId//
	public int aF;//addFriendValue//
	public CardJson[] cs0;//cards0//
	public CardJson[] cs1;//cards1//
	public String[] ds;//drops//  
	public int[] us0;//unitSkillsId0//
	public int[] us1;//unitSkillsId1//
	public int bNum;//战斗场次
	public int bs;//bonus奖励标识(0未领取过,1领取过)
	public int t;//==1有剧情,0无剧情==//
	public CardJson[] fs;//==好友战斗卡组角色==//
	public String fr;//==好友符文Id==//
	public int[] mes;//==双方最大怒气上限==//
	public String[] raceAtts;//==己方种族加成属性==//
	public int initE;//==初始怒气==//
	
	public int getMd()
	{
		return md;
	}
	public void setMd(int md)
	{
		this.md = md;
	}
	public int getaF()
	{
		return aF;
	}
	public void setaF(int aF)
	{
		this.aF = aF;
	}
	public CardJson[] getCs0()
	{
		return cs0;
	}
	public void setCs0(CardJson[] cs0)
	{
		this.cs0 = cs0;
	}
	public CardJson[] getCs1()
	{
		return cs1;
	}
	public void setCs1(CardJson[] cs1)
	{
		this.cs1 = cs1;
	}
	public String[] getDs()
	{
		return ds;
	}
	public void setDs(String[] ds)
	{
		this.ds = ds;
	}
	public int[] getUs0()
	{
		return us0;
	}
	public void setUs0(int[] us0)
	{
		this.us0 = us0;
	}
	public int[] getUs1()
	{
		return us1;
	}
	public void setUs1(int[] us1)
	{
		this.us1 = us1;
	}
	public int getBNum() {
		return bNum;
	}
	public void setBNum(int num) {
		bNum = num;
	}
	public int getT()
	{
		return t;
	}
	public void setT(int t)
	{
		this.t = t;
	}
	public int getBs() {
		return bs;
	}
	public void setBs(int bs) {
		this.bs = bs;
	}
	public CardJson[] getFs()
	{
		return fs;
	}
	public void setFs(CardJson[] fs)
	{
		this.fs = fs;
	}
	public String getFr()
	{
		return fr;
	}
	public void setFr(String fr)
	{
		this.fr = fr;
	}
	public int[] getMes()
	{
		return mes;
	}
	public void setMes(int[] mes)
	{
		this.mes = mes;
	}
	public String[] getRaceAtts()
	{
		return raceAtts;
	}
	public void setRaceAtts(String[] raceAtts)
	{
		this.raceAtts = raceAtts;
	}
	public int getInitE()
	{
		return initE;
	}
	public void setInitE(int initE)
	{
		this.initE = initE;
	}
	
}
