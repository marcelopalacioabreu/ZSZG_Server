package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class ComposeResultJson extends ErrorJson {
	public List<String> cs;//id-是否可制作-是否可解锁    可以为1，不可以为0
	public List<PackElement> pes;
	
//	public String s;//新解锁的物品id,id,id
	public List<String> mark; //种类-type(0为没有新题型，1为有新提醒)
	/**合成成功为0，失败为-1**/
	public int t;

	public List<String> getCs()
	{
		return cs;
	}

	public void setCs(List<String> cs)
	{
		this.cs = cs;
	}

	public List<PackElement> getPes()
	{
		return pes;
	}

	public void setPes(List<PackElement> pes)
	{
		this.pes = pes;
	}
	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}

	public List<String> getMark() {
		return mark;
	}

	public void setMark(List<String> mark) {
		this.mark = mark;
	}
	
}
