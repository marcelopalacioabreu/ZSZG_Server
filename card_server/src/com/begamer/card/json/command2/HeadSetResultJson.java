package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class HeadSetResultJson extends ErrorJson
{
	public int f;//友情值
	public int crystal;//水晶值
	public int crystals;
	public int iconId;//头像解锁id
	public String name;
	public int playerid;
	
	public int getF() {
		return f;
	}
	public void setF(int f) {
		this.f = f;
	}
	public int getCrystal() {
		return crystal;
	}
	public void setCrystal(int crystal) {
		this.crystal = crystal;
	}
	public int getIconId() {
		return iconId;
	}
	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
	public int getCrystals() {
		return crystals;
	}
	public void setCrystals(int crystals) {
		this.crystals = crystals;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPlayerid() {
		return playerid;
	}
	public void setPlayerid(int playerid) {
		this.playerid = playerid;
	}
}
