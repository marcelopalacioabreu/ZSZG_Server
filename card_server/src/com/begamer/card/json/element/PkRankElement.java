package com.begamer.card.json.element;

import com.begamer.card.model.pojo.Player;

public class PkRankElement
{
	public String icon;
	public String name;
	public int battlePower;
	public int rank;//排名
	public int award;//符文值奖励
	public int honor;//荣誉值奖励
	
	public void setData(Player p , int rank , int award ,int honor)
	{
		this.icon =p.getHead();
		this.name =p.getName();
		this.battlePower =p.getBattlePower();
		this.rank =rank;
		this.award =award;
		this.honor = honor;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getAward() {
		return award;
	}
	public void setAward(int award) {
		this.award = award;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBattlePower() {
		return battlePower;
	}
	public void setBattlePower(int battlePower) {
		this.battlePower = battlePower;
	}
	public int getHonor() {
		return honor;
	}
	public void setHonor(int honor) {
		this.honor = honor;
	}
	
}
