package com.begamer.card.model.dbservice.impl;

import com.begamer.card.common.Constant;

public abstract class AbstractService {

	protected int getPagedIndex(int pageID, int pageSize) {
		int index = (pageID - 1) * pageSize;
		if (index < 0)
			index = 0;
		return index;
	}

	protected int getPagedLimit(int pageID, int pageSize) {
		int limit = pageSize;
		if (limit <= 0)
			limit = Constant.LOAD_ALL;
		return limit;
	}

}
