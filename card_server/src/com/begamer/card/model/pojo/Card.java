package com.begamer.card.model.pojo;

import org.apache.log4j.Logger;

import com.begamer.card.cache.Cache;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.common.util.binRead.CardExpData;
import com.begamer.card.common.util.binRead.EvolutionData;
import com.begamer.card.log.PlayerLogger;

/**
 * Card
 * @author LiTao
 * 2013-8-16 09:46:03
 */
public class Card
{
	private static final Logger playerLogger=PlayerLogger.logger;
	
	private int id;
	private int playerId;
	private int cardId;
	private int level;
	private int curExp;
	private int breakNum;
	private int breakType;
	private String createTime;
	private int sell;
	private int newType;
	
	public static Card createCard(int playerId,int cardId,int level)
	{
		Card c=new Card();
		c.setCreateTime(System.currentTimeMillis()+"");
		c.setCardId(cardId);
		c.setLevel(level);
		c.setPlayerId(playerId);
		c.setSell(0);
		c.setCurExp(0);
		c.setBreakNum(0);
		c.setNewType(0);
		return c;
	}
	
	public Card()
	{}
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(Integer playerId)
	{
		this.playerId = playerId;
	}
	public Integer getCardId()
	{
		return cardId;
	}
	public void setCardId(Integer cardId)
	{
		this.cardId = cardId;
	}
	public Integer getLevel()
	{
		return level;
	}
	public void setLevel(Integer level)
	{
		this.level = level;
	}
	public Integer getCurExp()
	{
		return curExp;
	}
	public void setCurExp(Integer curExp)
	{
		this.curExp = curExp;
	}
	public Integer getBreakNum()
	{
		return breakNum;
	}
	public void setBreakNum(Integer breakNum)
	{
		this.breakNum = breakNum;
	}

	public String getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}

	public Integer getSell() {
		return sell;
	}

	public void setSell(Integer sell) {
		this.sell = sell;
	}
	
	public int getBreakType() {
		return breakType;
	}

	public void setBreakType(int breakType) {
		this.breakType = breakType;
	}

	public void addExp(int exp)
	{
		playerLogger.info("|区服："+Cache.getInstance().serverId+"|角色卡："+cardId+"|等级："+level+"|获得经验|"+exp);
		CardData cd=CardData.getData(cardId);
		int mLevel=cd.maxLevel;
		PlayerInfo pi =Cache.getInstance().getPlayerInfo(playerId);
		if(breakNum>0)
		{
			EvolutionData evolutionData =EvolutionData.getData(cd.star, breakNum);
			mLevel =mLevel+evolutionData.lvl;
		}
		if(cd==null)
		{
			return;
		}
		while(exp>0)
		{
			CardExpData ced=CardExpData.getData(level+1);
			if(ced!=null)
			{
				if(exp+curExp>=ced.starexps[cd.star-1])
				{
					exp=exp+curExp-ced.starexps[cd.star-1];
					if(level<mLevel)
					{
						level+=1;
						//更新成就
						pi.player.updateAchieve(13, level+"");
					}
					else
					{
						curExp=0;
						break;
					}
					curExp=0;
				}
				else
				{
					curExp+=exp;
					exp=0;
				}
			}
			else
			{
				exp=0;
			}
		}
	}
	
	public int getTalent1()
	{
		CardData cd=CardData.getData(cardId);
		return cd.talent;
	}
	
	public int getTalent2()
	{
		if(breakNum>=Constant.SecondBreakNum)
		{
			CardData cd=CardData.getData(cardId);
			return cd.talent2;
		}
		return 0;
	}

	public int getTalent3()
	{
		if(breakNum==Constant.MaxBreakNum)
		{
			CardData cd=CardData.getData(cardId);
			return cd.talent3;
		}
		return 0;
	}
	
	public int getNewType() {
		return newType;
	}

	public void setNewType(int newType) {
		this.newType = newType;
	}
	
}
