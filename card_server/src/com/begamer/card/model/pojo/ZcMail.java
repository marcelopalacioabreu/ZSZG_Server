package com.begamer.card.model.pojo;

public class ZcMail {

	private int id;
	private String title;
	private String content;
	private String reward1;// 格式:type&id&number type:1item 2equip 3card 4skill 5pskill
	private String reward2;
	private String reward3;
	private String reward4;
	private String reward5;
	private String reward6;
	private int gold;
	private int crystal;
	private int runeNum;
	private int power;
	private int friendNum;
	private int state;// 1开启,0关闭
	private int deleteTime;// 单位分钟
	private int type;//1首次登陆邮件,2首次充值邮件

	public static ZcMail createFirstLoginMail(String title, String content,String reward1, String reward2, String reward3, String reward4,String reward5,String reward6,int gold,int crystal, int runeNum, int power, int friendNum, int deleteTime)
	{
		ZcMail mail = new ZcMail();
		mail.setTitle(title);
		mail.setContent(content);
		mail.setReward1(reward1);
		mail.setReward2(reward2);
		mail.setReward3(reward3);
		mail.setReward4(reward4);
		mail.setReward5(reward5);
		mail.setReward6(reward6);
		mail.setGold(gold);
		mail.setCrystal(crystal);
		mail.setRuneNum(runeNum);
		mail.setPower(power);
		mail.setFriendNum(friendNum);
		mail.setState(0);
		mail.setDeleteTime(deleteTime);
		mail.setType(1);
		return mail;
	}

	public static ZcMail createFirstPayMail(String title, String content,String reward1, String reward2, String reward3, String reward4,String reward5,String reward6,int gold,int crystal, int runeNum, int power, int friendNum, int deleteTime)
	{
		ZcMail mail = new ZcMail();
		mail.setTitle(title);
		mail.setContent(content);
		mail.setReward1(reward1);
		mail.setReward2(reward2);
		mail.setReward3(reward3);
		mail.setReward4(reward4);
		mail.setReward5(reward5);
		mail.setReward6(reward6);
		mail.setGold(gold);
		mail.setCrystal(crystal);
		mail.setRuneNum(runeNum);
		mail.setPower(power);
		mail.setFriendNum(friendNum);
		mail.setState(0);
		mail.setDeleteTime(deleteTime);
		mail.setType(2);
		return mail;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public String getReward1()
	{
		return reward1;
	}

	public void setReward1(String reward1)
	{
		this.reward1 = reward1;
	}

	public String getReward2()
	{
		return reward2;
	}

	public void setReward2(String reward2)
	{
		this.reward2 = reward2;
	}

	public String getReward3()
	{
		return reward3;
	}

	public void setReward3(String reward3)
	{
		this.reward3 = reward3;
	}

	public String getReward4()
	{
		return reward4;
	}

	public void setReward4(String reward4)
	{
		this.reward4 = reward4;
	}

	public String getReward5()
	{
		return reward5;
	}

	public void setReward5(String reward5)
	{
		this.reward5 = reward5;
	}

	public String getReward6()
	{
		return reward6;
	}

	public void setReward6(String reward6)
	{
		this.reward6 = reward6;
	}

	public int getGold()
	{
		return gold;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getCrystal()
	{
		return crystal;
	}

	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}

	public int getRuneNum()
	{
		return runeNum;
	}

	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}

	public int getPower()
	{
		return power;
	}

	public void setPower(int power)
	{
		this.power = power;
	}

	public int getFriendNum()
	{
		return friendNum;
	}

	public void setFriendNum(int friendNum)
	{
		this.friendNum = friendNum;
	}

	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public int getDeleteTime()
	{
		return deleteTime;
	}

	public void setDeleteTime(int deleteTime)
	{
		this.deleteTime = deleteTime;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

}
