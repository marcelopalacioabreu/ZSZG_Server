package com.begamer.card.model.pojo;

public class Announce {

	private int id;
	
	private String announce;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getAnnounce()
	{
		return announce;
	}

	public void setAnnounce(String announce)
	{
		this.announce = announce;
	}
	
	
}
